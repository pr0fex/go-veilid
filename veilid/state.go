package veilid

/*
#include "../veilid.h"
*/
import "C"

type LogLevel int

const (
	LogLevelError LogLevel = C.VLD_LOG_LEVEL_ERROR
	LogLevelWarn           = C.VLD_LOG_LEVEL_WARN
	LogLevelInfo           = C.VLD_LOG_LEVEL_INFO
	LogLevelDebug          = C.VLD_LOG_LEVEL_DEBUG
	LogLevelTrace          = C.VLD_LOG_LEVEL_TRACE
)

func (l LogLevel) String() string {
	name := C.vld_log_level_name(C.VLD_LogLevel(l))
	return C.GoString(name)
}

type UpdateLog struct {
	LogLevel  LogLevel `json:"log_level"`
	Message   string   `json:"message"`
	Backtrace string   `json:"backtrace"`
}

func goUpdateLog(log C.VLD_Log) UpdateLog {
	backtrace := ""
	if log.backtrace != nil {
		backtrace = C.GoString(log.backtrace)
	}

	return UpdateLog{
		LogLevel:  LogLevel(log.log_level),
		Message:   C.GoString(log.message),
		Backtrace: backtrace,
	}
}

type ConfigCapabilities struct {
	Disable []CryptoKind `json:"disable"`
}

func goConfigCapabilities(capabilities C.VLD_ConfigCapabilities) ConfigCapabilities {
	kind := C.vld_crypto_kind_array_data(capabilities.disable)
	size := C.vld_crypto_kind_array_len(capabilities.disable)
	disable := arrayToSlice(kind, int(size), goCryptoKind)
	return ConfigCapabilities{
		Disable: disable,
	}
}

type ConfigProtectedStore struct {
	AllowInsecureFallback          bool   `json:"allow_insecure_fallback"`
	AlwaysUseInsecureStorage       bool   `json:"always_use_insecure_storage"`
	Directory                      string `json:"directory"`
	Delete                         bool   `json:"delete"`
	DeviceEncryptionKeyPassword    string `json:"device_encryption_key_password"`
	NewDeviceEncryptionKeyPassword string `json:"new_device_encryption_key_password"`
}

func goConfigProtectedStore(store C.VLD_ConfigProtectedStore) ConfigProtectedStore {
	newDeviceEncryptionKeyPassword := ""
	if store.new_device_encryption_key_password != nil {
		newDeviceEncryptionKeyPassword = C.GoString(store.new_device_encryption_key_password)
	}

	return ConfigProtectedStore{
		AllowInsecureFallback:          bool(store.allow_insecure_fallback),
		AlwaysUseInsecureStorage:       bool(store.always_use_insecure_storage),
		Directory:                      C.GoString(store.directory),
		Delete:                         bool(store.delete_),
		DeviceEncryptionKeyPassword:    C.GoString(store.device_encryption_key_password),
		NewDeviceEncryptionKeyPassword: newDeviceEncryptionKeyPassword,
	}
}

type ConfigTableStore struct {
	Directory string `json:"directory"`
	Delete    bool   `json:"delete"`
}

func goConfigTableStore(store C.VLD_ConfigTableStore) ConfigTableStore {
	return ConfigTableStore{
		Directory: C.GoString(store.directory),
		Delete:    bool(store.delete_),
	}
}

type ConfigBlockStore struct {
	Directory string `json:"directory"`
	Delete    bool   `json:"delete"`
}

func goConfigBlockStore(store C.VLD_ConfigBlockStore) ConfigBlockStore {
	return ConfigBlockStore{
		Directory: C.GoString(store.directory),
		Delete:    bool(store.delete_),
	}
}

type ConfigRoutingTable struct {
	NodeID              TypedKeyGroup    `json:"node_id"`
	NodeIDSecret        TypedSecretGroup `json:"node_id_secret"`
	Bootstrap           []string         `json:"bootstrap"`
	LimitOverAttached   uint32           `json:"limit_over_attached"`
	LimitFullyAttached  uint32           `json:"limit_fully_attached"`
	LimitAttachedStrong uint32           `json:"limit_attached_strong"`
	LimitAttachedGood   uint32           `json:"limit_attached_good"`
	LimitAttachedWeak   uint32           `json:"limit_attached_weak"`
}

func goConfigRoutingTable(routingTable C.VLD_ConfigRoutingTable) ConfigRoutingTable {
	return ConfigRoutingTable{}
}

type ConfigRPC struct {
	Concurrency          uint32  `json:"concurrency"`
	QueueSize            uint32  `json:"queue_size"`
	MaxTimestampBehindMS *uint32 `json:"max_timestamp_behind_ms"`
	MaxTimestampAheadMS  *uint32 `json:"max_timestamp_ahead_ms"`
	TimeoutMS            uint32  `json:"timeout_ms"`
	MaxRouteHopCount     uint8   `json:"max_route_hop_count"`
	DefaultRouteHopCount uint8   `json:"default_route_hop_count"`
}

func goConfigRPC(rpc C.VLD_ConfigRpc) ConfigRPC {
	var maxTimestampBehindMS *uint32
	if rpc.max_timestamp_behind_ms != nil {
		maxTimestampBehindMS = new(uint32)
		*maxTimestampBehindMS = uint32(*rpc.max_timestamp_behind_ms)
	}

	var maxTimestampAheadMS *uint32
	if rpc.max_timestamp_ahead_ms != nil {
		maxTimestampAheadMS = new(uint32)
		*maxTimestampAheadMS = uint32(*rpc.max_timestamp_ahead_ms)
	}

	return ConfigRPC{
		Concurrency:          uint32(rpc.concurrency),
		QueueSize:            uint32(rpc.queue_size),
		MaxTimestampBehindMS: maxTimestampBehindMS,
		MaxTimestampAheadMS:  maxTimestampAheadMS,
		TimeoutMS:            uint32(rpc.timeout_ms),
		MaxRouteHopCount:     uint8(rpc.max_route_hop_count),
		DefaultRouteHopCount: uint8(rpc.default_route_hop_count),
	}
}

type ConfigDHT struct {
	MaxFindNodeCount              uint32 `json:"max_find_node_count"`
	ResolveNodeTimeoutMS          uint32 `json:"resolve_node_timeout_ms"`
	ResolveNodeCount              uint32 `json:"resolve_node_count"`
	ResolveNodeFanout             uint32 `json:"resolve_node_fanout"`
	GetValueTimeoutMS             uint32 `json:"get_value_timeout_ms"`
	GetValueCount                 uint32 `json:"get_value_count"`
	GetValueFanout                uint32 `json:"get_value_fanout"`
	SetValueTimeoutMS             uint32 `json:"set_value_timeout_ms"`
	SetValueCount                 uint32 `json:"set_value_count"`
	SetValueFanout                uint32 `json:"set_value_fanout"`
	MinPeerCount                  uint32 `json:"min_peer_count"`
	MinPeerRefreshTimeMS          uint32 `json:"min_peer_refresh_time_ms"`
	ValidateDialInfoReceiptTimeMS uint32 `json:"validate_dial_info_receipt_time_ms"`
	LocalSubkeyCacheSize          uint32 `json:"local_subkey_cache_size"`
	LocalMaxSubkeyCacheMemoryMB   uint32 `json:"local_max_subkey_cache_memory_mb"`
	RemoteSubkeyCacheSize         uint32 `json:"remote_subkey_cache_size"`
	RemoteMaxRecords              uint32 `json:"remote_max_records"`
	RemoteMaxSubkeyCacheMemoryMB  uint32 `json:"remote_max_subkey_cache_memory_mb"`
	RemoteMaxStorageSpaceMB       uint32 `json:"remote_max_storage_space_mb"`
}

func goConfigDHT(dht C.VLD_ConfigDht) ConfigDHT {
	return ConfigDHT{
		MaxFindNodeCount:              uint32(dht.max_find_node_count),
		ResolveNodeTimeoutMS:          uint32(dht.resolve_node_timeout_ms),
		ResolveNodeCount:              uint32(dht.resolve_node_count),
		ResolveNodeFanout:             uint32(dht.resolve_node_fanout),
		GetValueTimeoutMS:             uint32(dht.get_value_timeout_ms),
		GetValueCount:                 uint32(dht.get_value_count),
		GetValueFanout:                uint32(dht.get_value_fanout),
		SetValueTimeoutMS:             uint32(dht.set_value_timeout_ms),
		SetValueCount:                 uint32(dht.set_value_count),
		SetValueFanout:                uint32(dht.set_value_fanout),
		MinPeerCount:                  uint32(dht.min_peer_count),
		MinPeerRefreshTimeMS:          uint32(dht.min_peer_refresh_time_ms),
		ValidateDialInfoReceiptTimeMS: uint32(dht.validate_dial_info_receipt_time_ms),
		LocalSubkeyCacheSize:          uint32(dht.local_subkey_cache_size),
		LocalMaxSubkeyCacheMemoryMB:   uint32(dht.local_max_subkey_cache_memory_mb),
		RemoteSubkeyCacheSize:         uint32(dht.remote_subkey_cache_size),
		RemoteMaxSubkeyCacheMemoryMB:  uint32(dht.remote_max_subkey_cache_memory_mb),
		RemoteMaxStorageSpaceMB:       uint32(dht.remote_max_storage_space_mb),
	}
}

type ConfigTLS struct {
	CertifcatePath             string `json:"certificate_path"`
	PrivateKeyPath             string `json:"private_key_path"`
	ConnectionInitialTimeoutMS uint32 `json:"connection_initial_timeout_ms"`
}

func goConfigTLS(tls C.VLD_ConfigTls) ConfigTLS {
	return ConfigTLS{
		CertifcatePath:             C.GoString(tls.certificate_path),
		PrivateKeyPath:             C.GoString(tls.private_key_path),
		ConnectionInitialTimeoutMS: uint32(tls.connection_initial_timeout_ms),
	}
}

type ConfigHTTPS struct {
	Enabled       bool   `json:"enabled"`
	ListenAddress string `json:"listen_address"`
	Path          string `json:"path"`
	URL           string `json:"url"`
}

func goConfigHTTPS(https C.VLD_ConfigHttps) ConfigHTTPS {
	url := ""
	if https.url != nil {
		url = C.GoString(https.url)
	}

	return ConfigHTTPS{
		Enabled:       bool(https.enabled),
		ListenAddress: C.GoString(https.listen_address),
		Path:          C.GoString(https.path),
		URL:           url,
	}
}

type ConfigHTTP struct {
	Enabled       bool   `json:"enabled"`
	ListenAddress string `json:"listen_address"`
	Path          string `json:"path"`
	URL           string `json:"url"`
}

func goConfigHTTP(http C.VLD_ConfigHttp) ConfigHTTP {
	url := ""
	if http.url != nil {
		url = C.GoString(http.url)
	}

	return ConfigHTTP{
		Enabled:       bool(http.enabled),
		ListenAddress: C.GoString(http.listen_address),
		Path:          C.GoString(http.path),
		URL:           url,
	}
}

type ConfigApplication struct {
	HTTPS ConfigHTTPS `json:"https"`
	HTTP  ConfigHTTP  `json:"http"`
}

func goConfigApplication(application C.VLD_ConfigApplication) ConfigApplication {
	return ConfigApplication{
		HTTPS: goConfigHTTPS(application.https),
		HTTP:  goConfigHTTP(application.http),
	}
}

type ConfigUDP struct {
	Enabled        bool   `json:"enabled"`
	SocketPoolSize uint32 `json:"socket_pool_size"`
	ListenAddress  string `json:"listen_address"`
	PublicAddress  string `json:"public_address"`
}

func goConfigUDP(udp C.VLD_ConfigUdp) ConfigUDP {
	publicAddress := ""
	if udp.public_address != nil {
		publicAddress = C.GoString(udp.public_address)
	}

	return ConfigUDP{
		Enabled:        bool(udp.enabled),
		SocketPoolSize: uint32(udp.socket_pool_size),
		ListenAddress:  C.GoString(udp.listen_address),
		PublicAddress:  publicAddress,
	}
}

type ConfigTCP struct {
	Connect        bool   `json:"connect"`
	Listen         bool   `json:"listen"`
	MaxConnections uint32 `json:"max_connections"`
	ListenAddress  string `json:"listen_address"`
	PublicAddress  string `json:"public_address"`
}

func goConfigTCP(tcp C.VLD_ConfigTcp) ConfigTCP {
	publicAddress := ""
	if tcp.public_address != nil {
		publicAddress = C.GoString(tcp.public_address)
	}

	return ConfigTCP{
		Connect:        bool(tcp.connect),
		Listen:         bool(tcp.listen),
		MaxConnections: uint32(tcp.max_connections),
		ListenAddress:  C.GoString(tcp.listen_address),
		PublicAddress:  publicAddress,
	}
}

type ConfigWS struct {
	Connect        bool   `json:"connect"`
	Listen         bool   `json:"listen"`
	MaxConnections uint32 `json:"max_connections"`
	ListenAddress  string `json:"listen_address"`
	Path           string `json:"path"`
	URL            string `json:"url"`
}

func goConfigWS(ws C.VLD_ConfigWs) ConfigWS {
	url := ""
	if ws.url != nil {
		url = C.GoString(ws.url)
	}

	return ConfigWS{
		Connect:        bool(ws.connect),
		Listen:         bool(ws.listen),
		MaxConnections: uint32(ws.max_connections),
		ListenAddress:  C.GoString(ws.listen_address),
		Path:           C.GoString(ws.path),
		URL:            url,
	}
}

type ConfigWSS struct {
	Connect        bool   `json:"connect"`
	Listen         bool   `json:"listen"`
	MaxConnections uint32 `json:"max_connections"`
	ListenAddress  string `json:"listen_address"`
	Path           string `json:"path"`
	URL            string `json:"url"`
}

func goConfigWSS(wss C.VLD_ConfigWss) ConfigWSS {
	url := ""
	if wss.url != nil {
		url = C.GoString(wss.url)
	}

	return ConfigWSS{
		Connect:        bool(wss.connect),
		Listen:         bool(wss.listen),
		MaxConnections: uint32(wss.max_connections),
		ListenAddress:  C.GoString(wss.listen_address),
		Path:           C.GoString(wss.path),
		URL:            url,
	}
}

type ConfigProtocol struct {
	UDP ConfigUDP `json:"udp"`
	TCP ConfigTCP `json:"tcp"`
	WS  ConfigWS  `json:"ws"`
	WSS ConfigWSS `json:"wss"`
}

func goConfigProtocol(protocol C.VLD_ConfigProtocol) ConfigProtocol {
	return ConfigProtocol{
		UDP: goConfigUDP(protocol.udp),
		TCP: goConfigTCP(protocol.tcp),
		WS:  goConfigWS(protocol.ws),
		WSS: goConfigWSS(protocol.wss),
	}
}

type ConfigNetwork struct {
	ConnectionInitialTimeoutMS      uint32             `json:"connection_initial_timeout_ms"`
	ConnectionInactivityTimeoutMS   uint32             `json:"connection_inactivity_timeout_ms"`
	MaxConnectionsPerIPv4           uint32             `json:"max_connections_per_ip4"`
	MaxConnectionsPerIPv6Prefix     uint32             `json:"max_connections_per_ip6_prefix"`
	MaxConnectionsPerIPv6PrefixSize uint32             `json:"max_connections_per_ip6_prefix_size"`
	MaxConnectionFrequencyPerMin    uint32             `json:"max_connection_frequency_per_min"`
	ClientWhitelistTimeoutMS        uint32             `json:"client_whitelist_timeout_ms"`
	ReverseConnectionReceiptTimeMS  uint32             `json:"reverse_connection_receipt_time_ms"`
	HolePunchReceiptTimeMS          uint32             `json:"hole_punch_receipt_time_ms"`
	NetworkKeyPassword              string             `json:"network_key_password"`
	RoutingTable                    ConfigRoutingTable `json:"routing_table"`
	RPC                             ConfigRPC          `json:"rpc"`
	DHT                             ConfigDHT          `json:"dht"`
	UPNP                            bool               `json:"upnp"`
	DetectAddressChanges            bool               `json:"detect_address_changes"`
	RestrictedNATRetries            uint32             `json:"restricted_nat_retries"`
	TLS                             ConfigTLS          `json:"tls"`
	Application                     ConfigApplication  `json:"application"`
	Protocol                        ConfigProtocol     `json:"protocol"`
}

func goConfigNetwork(network C.VLD_ConfigNetwork) ConfigNetwork {
	return ConfigNetwork{
		ConnectionInitialTimeoutMS:      uint32(network.connection_initial_timeout_ms),
		ConnectionInactivityTimeoutMS:   uint32(network.connection_inactivity_timeout_ms),
		MaxConnectionsPerIPv4:           uint32(network.max_connections_per_ip4),
		MaxConnectionsPerIPv6Prefix:     uint32(network.max_connections_per_ip6_prefix),
		MaxConnectionsPerIPv6PrefixSize: uint32(network.max_connections_per_ip6_prefix_size),
		MaxConnectionFrequencyPerMin:    uint32(network.max_connection_frequency_per_min),
		ClientWhitelistTimeoutMS:        uint32(network.client_whitelist_timeout_ms),
		ReverseConnectionReceiptTimeMS:  uint32(network.reverse_connection_receipt_time_ms),
		HolePunchReceiptTimeMS:          uint32(network.hole_punch_receipt_time_ms),
		NetworkKeyPassword:              C.GoString(network.network_key_password),
		RoutingTable:                    goConfigRoutingTable(network.routing_table),
		RPC:                             goConfigRPC(network.rpc),
		DHT:                             goConfigDHT(network.dht),
		UPNP:                            bool(network.upnp),
		DetectAddressChanges:            bool(network.detect_address_changes),
		TLS:                             goConfigTLS(network.tls),
		Application:                     goConfigApplication(network.application),
		Protocol:                        goConfigProtocol(network.protocol),
	}
}

type UpdateConfig struct {
	ProgramName    string               `json:"program_name"`
	Namespace      string               `json:"namespace"`
	Capabilities   ConfigCapabilities   `json:"capabilities"`
	ProtectedStore ConfigProtectedStore `json:"protected_store"`
	TableStore     ConfigTableStore     `json:"table_store"`
	BlockStore     ConfigBlockStore     `json:"block_store"`
	Network        ConfigNetwork        `json:"network"`
}

func goUpdateConfig(config C.VLD_Config) UpdateConfig {
	return UpdateConfig{
		ProgramName:    C.GoString(config.program_name),
		Namespace:      C.GoString(config.namespace_),
		Capabilities:   goConfigCapabilities(config.capabilities),
		ProtectedStore: goConfigProtectedStore(config.protected_store),
		TableStore:     goConfigTableStore(config.table_store),
		BlockStore:     goConfigBlockStore(config.block_store),
		Network:        goConfigNetwork(config.network),
	}
}

type UpdateAppMessage struct {
	Sender  *TypedKey `json:"sender"`
	Message []byte    `json:"message"`
}

func goUpdateAppMessage(appMessage C.VLD_AppMessage) UpdateAppMessage {
	var sender *TypedKey
	if appMessage.sender != nil {
		sender = new(TypedKey)
		*sender = goTypedKey(*appMessage.sender)
	}
	message := goByteArray(
		C.vld_byte_array_data(appMessage.message),
		int(C.vld_byte_array_len(appMessage.message)),
	)

	return UpdateAppMessage{
		Sender:  sender,
		Message: message,
	}
}

type UpdateAppCall struct {
	Sender  *TypedKey `json:"sender"`
	Message []byte    `json:"message"`
	ID      int       `json:"id"`
}

func goUpdateAppCall(appCall C.VLD_AppCall) UpdateAppCall {
	var sender *TypedKey
	if appCall.sender != nil {
		sender = new(TypedKey)
		*sender = goTypedKey(*appCall.sender)
	}
	message := goByteArray(
		C.vld_byte_array_data(appCall.message),
		int(C.vld_byte_array_len(appCall.message)),
	)
	return UpdateAppCall{
		Sender:  sender,
		Message: message,
		ID:      int(appCall.id),
	}
}

type AttachmentState int

const (
	AttachmentStateDetached       AttachmentState = C.VLD_ATTACHMENT_STATE_DETACHED
	AttachmentStateAttaching                      = C.VLD_ATTACHMENT_STATE_ATTACHING
	AttachmentStateAttachedWeak                   = C.VLD_ATTACHMENT_STATE_ATTACHED_WEAK
	AttachmentStateAttachedGood                   = C.VLD_ATTACHMENT_STATE_ATTACHED_GOOD
	AttachmentStateAttachedStrong                 = C.VLD_ATTACHMENT_STATE_ATTACHED_STRONG
	AttachmentStateFullyAttached                  = C.VLD_ATTACHMENT_STATE_FULLY_ATTACHED
	AttachmentStateOverAttached                   = C.VLD_ATTACHMENT_STATE_OVER_ATTACHED
	AttachmentStateDetaching                      = C.VLD_ATTACHMENT_STATE_DETACHING
)

type UpdateStateAttachment struct {
	State               AttachmentState `json:"state"`
	PublicInternetReady bool            `json:"public_internet_ready"`
	LocalNetworkReady   bool            `json:"local_network_ready"`
}

type UpdateShutdown struct{}

type UpdateCallback func(any)
