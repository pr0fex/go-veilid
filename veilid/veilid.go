package veilid

/*
#include "../veilid.h"

extern void go_vld_update_callback(VLD_Update*);
*/
import "C"
import (
	"io"
	"os"
	"runtime"
	"unsafe"
)

var updateCh chan<- Update = nil

//export updateCallbackCGo
func updateCallbackCGo(update *C.VLD_Update) {
	if updateCh == nil {
		return
	}

	switch update.tag {
	case C.VLD_UPDATE_CONFIG:
		data := *(*C.VLD_Config)(unsafe.Pointer(&update.anon0[0]))
		config := goUpdateConfig(data)
		updateCh <- config
	case C.VLD_UPDATE_SHUTDOWN:
		updateCh <- UpdateShutdown{}
	}
}

type Update = any

func Start(configPath string, updates chan<- Update) error {
	f, err := os.Open(configPath)
	if err != nil {
		return err
	}
	defer f.Close()

	config, err := io.ReadAll(f)
	if err != nil {
		return err
	}

	configStr := C.CString(string(config))
	defer C.free(unsafe.Pointer(configStr))

	runtime.LockOSThread()
	defer runtime.UnlockOSThread()

	if updateCh != nil {
		return newVeilidError("already initialized")
	}
	updateCh = updates

	result := C.vld_start(configStr, C.VLD_UpdateCallback(C.go_vld_update_callback))
	if result != C.VLD_RESULT_OK {
		return getLastError()
	}

	return nil
}

func Shutdown() error {
	runtime.LockOSThread()
	defer runtime.UnlockOSThread()

	if result := C.vld_shutdown(); result != C.VLD_RESULT_OK {
		return getLastError()
	}

	if updateCh != nil {
		close(updateCh)
	}

	return nil
}
