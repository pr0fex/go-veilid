package veilid

// #include "../veilid.h"
import "C"
import "fmt"

type VeilidError struct {
	Message string
}

func newVeilidError(message string) *VeilidError {
	return &VeilidError{
		Message: message,
	}
}

func getLastError() *VeilidError {
	message := C.vld_get_last_error()
	defer C.vld_string_free(&message)
	return newVeilidError(C.GoString(message))
}

func (e VeilidError) Error() string {
	return fmt.Sprintf("veilid: %s", e.Message)
}
