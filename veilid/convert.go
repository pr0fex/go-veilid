package veilid

// #include "../veilid.h"
import "C"

import "unsafe"

func arrayToSlice[T any, U any](ptr *T, size int, f func(T) U) []U {
	slice := make([]U, size)
	arr := unsafe.Slice(ptr, size)
	for i := 0; i < len(slice); i++ {
		slice = append(slice, f(arr[i]))
	}
	return slice
}

func goByteArray(ptr *C.uint8_t, size int) []byte {
	return arrayToSlice(ptr, size, func(b C.uint8_t) byte {
		return byte(b)
	})
}
