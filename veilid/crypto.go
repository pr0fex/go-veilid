package veilid

// #include "../veilid.h"
import "C"
import (
	"runtime"
	"unsafe"
)

type CryptoKind int

func goCryptoKind(kind C.VLD_CryptoKind) CryptoKind {
	return CryptoKind(kind)
}

const CryptoKeyLength int = C.VLD_CRYPTO_KEY_LENGTH
const SignatureLength int = C.VLD_SIGNATURE_LENGTH
const NonceLength int = C.VLD_NONCE_LENGTH

type cryptoKey struct {
	key C.VLD_CryptoKey
}

func goCryptoKey(key C.VLD_CryptoKey) cryptoKey {
	return cryptoKey{key}
}

func (k cryptoKey) Bytes() [CryptoKeyLength]byte {
	ptr := (*C.VLD_CryptoKey)(unsafe.Pointer(&k.key[0]))
	data := goByteArray(&ptr[0], CryptoKeyLength)
	var key [CryptoKeyLength]byte
	copy(key[:], data[:CryptoKeyLength])
	return key
}

func (k cryptoKey) String() string {
	encoded := C.vld_crypto_key_encode(&k.key)
	defer C.vld_string_free(&encoded)
	return C.GoString(encoded)
}

type CryptoKey interface {
	Bytes() [CryptoKeyLength]byte
	String() string
}

type PublicKey struct {
	cryptoKey
}

func goPublicKey(key C.VLD_PublicKey) PublicKey {
	return PublicKey{goCryptoKey(key)}
}

type SecretKey struct {
	cryptoKey
}

func goSecretKey(secret C.VLD_SecretKey) SecretKey {
	return SecretKey{goCryptoKey(secret)}
}

// type HashDigest struct {
// 	cryptoKey
// }

// func goHashDigest(hash C.VLD_HashDigest) HashDigest {
// 	return HashDigest{goCryptoKey(hash)}
// }

type SharedSecret struct {
	cryptoKey
}

func goSharedSecret(sharedSecret C.VLD_SharedSecret) SharedSecret {
	return SharedSecret{goCryptoKey(sharedSecret)}
}

type RouteID struct {
	cryptoKey
}

func goRouteID(routeID C.VLD_RouteId) RouteID {
	return RouteID{goCryptoKey(routeID)}
}

// type CryptoKeyDistance struct {
// 	cryptoKey
// }

// func goCryptoKeyDistance(distance C.VLD_CryptoKeyDistance) CryptoKeyDistance {
// 	return CryptoKeyDistance{goCryptoKey(distance)}
// }

type Keypair struct {
	keypair C.VLD_Keypair
}

func goKeypair(keypair C.VLD_Keypair) Keypair {
	return Keypair{keypair}
}

func (k Keypair) Key() PublicKey {
	return goPublicKey(k.keypair.key)
}

func (k Keypair) Secret() SecretKey {
	return goSecretKey(k.keypair.secret)
}

func (k Keypair) String() string {
	encoded := C.vld_keypair_encode(&k.keypair)
	defer C.vld_string_free(&encoded)
	return C.GoString(encoded)
}

type CryptoTyped[T any] interface {
	Kind() CryptoKind
	Value() T
	// String() string
}

type TypedKey struct {
	key C.VLD_TypedKey
}

func goTypedKey(key C.VLD_TypedKey) TypedKey {
	return TypedKey{key}
}

func DecodeTypedKey(encoded string) (TypedKey, error) {
	cstr := C.CString(encoded)
	defer C.free(unsafe.Pointer(cstr))

	var key C.VLD_TypedKey
	result := C.vld_typed_key_decode(cstr, &key)
	if result != C.VLD_RESULT_OK {
		return TypedKey{}, getLastError()
	}
	return goTypedKey(key), nil
}

func (k TypedKey) Kind() CryptoKind {
	return CryptoKind(k.key.kind)
}

func (k TypedKey) Value() PublicKey {
	return goPublicKey(k.key.value)
}

func (k TypedKey) String() string {
	encoded := C.vld_typed_key_encode(&k.key)
	defer C.vld_string_free(&encoded)
	return C.GoString(encoded)
}

type TypedSecret struct {
	secret C.VLD_TypedSecret
}

func goTypedSecret(secret C.VLD_TypedSecret) TypedSecret {
	return TypedSecret{secret}
}

func DecodeTypedSecret(encoded string) (TypedSecret, error) {
	cstr := C.CString(encoded)
	defer C.free(unsafe.Pointer(cstr))

	var key C.VLD_TypedSecret
	result := C.vld_typed_secret_decode(cstr, &key)
	if result != C.VLD_RESULT_OK {
		return TypedSecret{}, getLastError()
	}
	return goTypedSecret(key), nil
}

func (s TypedSecret) Kind() CryptoKind {
	return CryptoKind(s.secret.kind)
}

func (s TypedSecret) Value() SecretKey {
	return goSecretKey(s.secret.value)
}

func (s TypedSecret) String() string {
	encoded := C.vld_typed_secret_encode(&s.secret)
	defer C.vld_string_free(&encoded)
	return C.GoString(encoded)
}

type TypedKeypair struct {
	keypair C.VLD_TypedKeypair
}

func goTypedKeypair(keypair C.VLD_TypedKeypair) TypedKeypair {
	return TypedKeypair{keypair}
}

func DecodeTypedKeypair(encoded string) (TypedKeypair, error) {
	cstr := C.CString(encoded)
	defer C.free(unsafe.Pointer(cstr))

	var key C.VLD_TypedKeypair
	result := C.vld_typed_keypair_decode(cstr, &key)
	if result != C.VLD_RESULT_OK {
		return TypedKeypair{}, getLastError()
	}
	return goTypedKeypair(key), nil
}

func (k TypedKeypair) Kind() CryptoKind {
	return CryptoKind(k.keypair.kind)
}

func (k TypedKeypair) Value() Keypair {
	return goKeypair(k.keypair.value)
}

func (k TypedKeypair) String() string {
	encoded := C.vld_typed_keypair_encode(&k.keypair)
	defer C.vld_string_free(&encoded)
	return C.GoString(encoded)
}

type typedGroup[K CryptoKey, T CryptoTyped[K]] struct {
	items []T
}

func (t *typedGroup[K, T]) Kinds() CryptoKind {
	var kinds []CryptoKind
	for _, key := range t.items {
		kinds = append(kinds, key.Kind())
	}
	return CryptoKindVLD0
}

type TypedKeyGroup typedGroup[PublicKey, TypedKey]

func goTypedKeyGroup(group *C.VLD_TypedKeyGroup) TypedKeyGroup {
	arr := C.vld_typed_key_group_items(group)
	defer C.vld_typed_key_group_free(&group)

	data := C.vld_typed_key_array_data(arr)
	size := C.vld_typed_key_array_len(arr)

	return TypedKeyGroup{
		items: arrayToSlice(data, int(size), goTypedKey),
	}
}

type TypedSecretGroup typedGroup[SecretKey, TypedSecret]

func goTypedSecretGroup(group *C.VLD_TypedSecretGroup) TypedSecretGroup {
	arr := C.vld_typed_secret_group_items(group)
	defer C.vld_typed_secret_group_free(&group)

	data := C.vld_typed_secret_array_data(arr)
	size := C.vld_typed_secret_array_len(arr)

	return TypedSecretGroup{
		items: arrayToSlice(data, int(size), goTypedSecret),
	}
}

func RandomBytes(kind CryptoKind, buf []byte) error {
	runtime.LockOSThread()
	defer runtime.UnlockOSThread()

	result := C.vld_crypto_random_bytes(C.VLD_CryptoKind(kind), (*C.uint8_t)(&buf[0]), C.uint32_t(len(buf)))
	if result != C.VLD_RESULT_OK {
		return getLastError()
	}

	return nil
}

func GenerateKeypair(kind CryptoKind) (TypedKeypair, error) {
	var keypair C.VLD_TypedKeypair

	runtime.LockOSThread()
	runtime.UnlockOSThread()

	result := C.vld_generate_keypair(C.VLD_CryptoKind(kind), &keypair)
	if result != C.VLD_RESULT_OK {
		return TypedKeypair{}, getLastError()
	}

	return goTypedKeypair(keypair), nil
}
